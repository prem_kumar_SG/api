package API_Implementation;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import Utills.Report;


public class API {
	
	
	public boolean isobjPresent(WebElement obj) {
		try {
			if (obj.isDisplayed() || obj.isEnabled() || obj.isSelected())
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	public boolean isobjEnabled(WebElement obj) {
		try {
			if (obj.isEnabled())
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	public boolean fnWait(WebDriver driver , WebElement Obj, String ObjectName, int iterator) {
		boolean flag = false;
		for (int i = 0; i < iterator; i++) {

			if (waitObj(driver,Obj)) {
				flag = true;
				break;
			}
		}

		if (!flag)
			Report.Fail(driver, "Verify the element '" + ObjectName + "'is present"+" , Element '" + ObjectName + "' is not present after " + iterator + " seconds");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		return flag;
	}
	

	public boolean waitObj(WebDriver driver ,WebElement Obj) {
		boolean flag = false;
		try {
			driver.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);
			flag = isobjPresent(Obj);

		} catch (Exception e) {

		}

		return flag;
	}
	
	public boolean ClickFeild(WebDriver driver,WebElement obj, String ObjectName)
    {
		boolean clickElement = false;
        try
        {
            if (isobjPresent(obj))
            {
            	System.out.println("done");
            	obj.click();
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", obj);
				Report.Pass("Click the element - " + ObjectName + " , Clicked on - " + ObjectName);
                clickElement = true;
            }
            else
            {
            	Report.Fail(driver, "Click the element - " + ObjectName+ " Element is not found - " + ObjectName);
            }
        }
        catch (Exception e)
        {
           e.printStackTrace();
           Report.Fail(driver,"Click the element - " + ObjectName+ " Problem in Clicking on - " + ObjectName);
        }
        return clickElement;
    }
	
////////////////////////////////////////////

	public boolean EnterTextFeild(WebDriver driver, WebElement Obj, String ObjectName, String Value) 
	{
		boolean sendKeys = false;
		try {
			if (isobjPresent(Obj)) {
			//	ClearTextField(driver,Obj);
				Obj.sendKeys(Value);
				sendKeys = true;
				Report.Pass("Enter the text '" + Value + "' in the " + ObjectName + " Textbox " + " , Entered the text '" + Value + "' in the " + ObjectName + " Textbox");

			} else {
				Report.Fail(driver, "Enter the text '" + Value + "' in the " + ObjectName + " Textbox "+" , Element is not found - " + ObjectName + " Textbox");
			}

		} catch (Exception e) {

			e.printStackTrace();
			Report.Fail(driver,"Enter the text '" + Value + "' in the " + ObjectName + " Textbox"+" , Problem in entering the text '" + Value + "' in the " + ObjectName + " Textbox");
		}

return sendKeys;
}
	///////////////////////////
	public boolean EnterKeys(WebDriver driver, WebElement Obj, String ObjectName, Keys key_val) 
	{
		boolean sendKeys = false;
		try {
			if (isobjPresent(Obj)) {
				ClearTextField(driver,Obj);
				Obj.sendKeys(key_val);
				//Obj.sendKeys(Keys.);
				sendKeys = true;
				Report.Pass("Enter the text '" + key_val + "' in the " + ObjectName + " Textbox " + " , Entered the text '" + key_val + "' in the " + ObjectName + " Textbox");

			} else {
				Report.Fail(driver, "Enter the text '" + key_val + "' in the " + ObjectName + " Textbox "+" , Element is not found - " + ObjectName + " Textbox");
			}

		} catch (Exception e) {

			e.printStackTrace();
			Report.Fail(driver,"Enter the text '" + key_val + "' in the " + ObjectName + " Textbox"+" , Problem in entering the text '" + key_val + "' in the " + ObjectName + " Textbox");
		}

return sendKeys;
}
	/////////////////////////////
	
	 ////////////////////////////////////////////

    public boolean VerifyObjPresent(WebDriver driver, WebElement Obj, String ObjectName)
    {
    	boolean verifyObj = false;
        try
        {
            if (isobjPresent(Obj))
            {
            	Report.Pass("Verify the element '" + ObjectName + "'is present" + " , Element '" + ObjectName + "' presented");
                verifyObj = true;
            }
            else
            {
            	Report.Fail(driver, "Verify the element '" + ObjectName + "'is present" + " , Element '" + ObjectName + "' is not present");
            }
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        	Report.Fail(driver, "Verify the element '" + ObjectName + "'is present"+ " , Element '" + ObjectName + "' is not present");
        }
        return verifyObj;
    }
    

    ////////////////////////////////////////////
    
    public boolean VerifyObjNOTPresent(WebDriver driver, WebElement Obj, String ObjectName)
    {
    	boolean verifyNOTObj = false;
        try
        {
            if (!(Obj.isDisplayed() && Obj.isEnabled()))
            {
            	Report.Pass("Verify the element '" + ObjectName + "'is NOT present" + " , Element '" + ObjectName + "' NOT presented");
            	verifyNOTObj = true;
            }
            else
            {
            	Report.Fail(driver, "Verify the element '" + ObjectName + "'is present" + " , Element '" + ObjectName + "' is presented");
            }
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        	Report.Fail(driver, "Verify the element '" + ObjectName + "'is not present"+ " , Element '" + ObjectName + "' is present");
        }
        return verifyNOTObj;
    }

    ////////////////////////////////////////////
    
    public boolean SelectField(WebDriver driver, WebElement obj,String ObjectName, String option,String value) {
    	boolean Select = false;
    	try {
			
			if (isobjPresent(obj)) {

				System.out.println("Select field");

				Select dd = new Select(obj);

				if (option.equalsIgnoreCase("text")) {
					dd.selectByVisibleText(value);
				} else if (option.equalsIgnoreCase("index")) {
					int index = Integer.parseInt(value);
					dd.selectByIndex(index);
				} else {
					dd.selectByValue(value);
				}
				Select = true;
				Report.Pass("Select the dropdown " + ObjectName  + " , Selected the dropdown " + ObjectName );

				return Select;

			} else {
				Report.Fail(driver, "Select the dropdown " + ObjectName  + " , NOT Selected the dropdown " + ObjectName );
				return Select;
			}
		} catch (Exception exception) {
			Report.Fail(driver, "Select the dropdown " + ObjectName  + " , Problem in Select the dropdown " + ObjectName );
			return Select;
		}
	}
    


    ///////////////////////////////////////////////

    public boolean CompareTwoText(WebDriver driver , String CompareValue1, String CompareValue2, String ObjectName)
    {
    	boolean Compare = false;
        try
        {
            if (CompareValue1.equalsIgnoreCase(CompareValue2))
            {
                Compare = true;
                Report.Pass("Verify the '" + ObjectName + "' value matches " + CompareValue2 + " , '"+ ObjectName + "' value matches " + CompareValue1);
            }
            else
            {
            	Report.Fail(driver, "Verify the '" + ObjectName + "' value matches " + CompareValue2 +  " , '" + ObjectName + "' value does not match " + CompareValue1);
            }

        }
        catch (Exception e)
        {
        	e.printStackTrace();
        	Report.Fail(driver, "Verify the '" + ObjectName + "' value matches " + CompareValue2 +  " Problem in comparing the  " + ObjectName + "'  value with  " + CompareValue1);

        }
        return Compare;
    }

    ////////////////////////////////////////////
    public boolean Compare(WebDriver driver,WebElement Obj, String ObjectName, String CompareValue)
    {
    	boolean Compare = false;
        try
        {
            if (isobjPresent(Obj))
            {

                if (Obj.getText().equalsIgnoreCase(CompareValue))
                {
                	 Compare = true;
                     Report.Pass("Verify the '" + ObjectName + "' value matches " + CompareValue + " , '"+ ObjectName + "' value matches " + CompareValue);
                 }
                 else
                 {
                 	Report.Fail(driver, "Verify the '" + ObjectName + "' value matches " + CompareValue +  " , '" + ObjectName + "' value does not match " + CompareValue);
                 }

            }
            else
            {
            	Report.Fail(driver,"Verify the '" + ObjectName + "' value matches " + CompareValue + " The '" + ObjectName + "' is not present");
            }
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        	Report.Fail(driver,"Verify the '" + ObjectName + "' value matches " + CompareValue + " Problem in comparing the '" + ObjectName + "' value with " + CompareValue);
        }
        return Compare;
    }
////////////////////////////////////////////
    
    public String fnGetText(WebDriver driver, WebElement obj) {
		String strActual = "";
		try {

			strActual = obj.getText().trim();

		} catch (Exception ex) {
			Report.Fail(driver, "Element not visible");
		}
		return strActual;
	}
    
////////////////////////////////////////////////
    public boolean ContainsText(WebDriver driver ,WebElement Obj , String ObjectName, String CompareValue)
    {
        boolean ContainsText = false;
        try
        {
            if (isobjPresent(Obj))
            {

            	String tmp = Obj.getText();
System.out.println(tmp);            	
                if (tmp.toLowerCase().contains(CompareValue.toLowerCase()))
                {
                    ContainsText = true;
                    Report.Pass("Passed : Verify the '" + ObjectName + "' value contains " + CompareValue + "'" + ObjectName + "' value contains " + CompareValue);
                }
                else
                {
                	Report.Fail(driver, "Failed : Verify the '" + ObjectName + "' value contains " + CompareValue + "'" + ObjectName + "' value does not contains " + CompareValue);
                }

            }
            else
            {
            	Report.Fail(driver,"Verify the '" + ObjectName + "' value contains " + CompareValue + " The '" + ObjectName + "' is not present");
            }
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        	Report.Fail(driver,"Verify the '" + ObjectName + "' value contains " + CompareValue + " Problem in comparing the '" + ObjectName + "' value with " + CompareValue);
        }
        return ContainsText;
    }
    
	///////////////////////////////////////////////
	public boolean ContainsTwoText(WebDriver driver,  String FullText, String ContainsText, String ObjectName) {
		boolean Contains = false;
		try {

				if (FullText.toLowerCase().contains(ContainsText.toLowerCase())) {
					Contains = true;
					Report.Pass("Passed : Verify the '" + ObjectName + "' value contains "+ FullText + " '" + ObjectName + "' value contains " + ContainsText);
				} else {
					Report.Fail(driver, "Failed : Verify the '" + ObjectName + "' value contains "+ FullText + "'" + ObjectName + "' value does not contains " + ContainsText);
				}

			 
		} catch (Exception e) {
			e.printStackTrace();
			Report.Fail(driver, "Verify the '" + ObjectName + "' value contains " + FullText+ " Problem in comparing the '" + ObjectName + "' value with " + ContainsText);
		}
		return Contains;
	}

    
	////////////////////////////////////////////////
	public boolean ClearTextField(WebDriver driver, WebElement Obj) 
	{
		try {

			Obj.clear();
			Obj.sendKeys("");
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
	
////////////////////////////////////////////
	
	//To upload files 
		public boolean fnUpload(String strFilename) {
			boolean upload = false;
			try {
				String browsername,ImgPath,Uploadpath;
				File file;
				
				file = new File("../../Upload-Script/Upload.exe");
				Uploadpath = file.getCanonicalPath();
				
				file = new File("../../Student_images/"+strFilename);
				ImgPath = file.getCanonicalPath();
				
				String strCmd = Uploadpath + " " + ImgPath ;
				System.out.println("Image file" + strCmd);
				
				Process process;
				
				try {
					process=Runtime.getRuntime().exec(strCmd);
					process.waitFor();
					upload = true;
				} catch (IOException e) {
					e.printStackTrace();
					upload = false;
				}
				

			} catch (Exception ex) {
				ex.printStackTrace();
				upload = false;
			}
			
			return upload;
		}


		public String fnGetAttribute(WebDriver driver, WebElement obj, String strAttribute) {
			String value = "";
			try {
				value = obj.getAttribute(strAttribute);
			} catch (Exception ex) {
				Report.Fail(driver, "Element not visible");

			}
			return value;
		}

		public API launchClientURL(WebDriver driver, String clientName, String browsername) {

			if (Report.failStep == 0) {
				Report.failStep = 0;
				Report.excelLog = "Failed Steps :\n";
				Report.testFailed = "Passed";
			}
			String url; // = "http://" + clientName;

			url = "http://" + clientName;
			try {
				driver.get(url);
				if (browsername.equalsIgnoreCase("chrome")) {
					Thread.sleep(5000);

				}

				System.out.println("URL found : " + driver.getTitle());
			} catch (Exception e) {
				e.printStackTrace();
				// Report.failStep=0;
				Report.Fail(driver, url + " URL is not available");
				//Assert.fail(driver.getTitle());
			}

			return PageFactory.initElements(driver, API.class);
		}
		
		public WebElement dynamicXpath(WebDriver driver, String xpath) {
			WebElement element = null;
			try {
				element = driver.findElement(By.xpath(xpath)); 
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			
			return element; 
	    }
		

		public void ActionEnterText(WebDriver driver, String strProperty, String value) {
			try {
				Actions action = new Actions(driver);
				WebElement element = driver.findElement(By.xpath(strProperty));
				action.moveToElement(element).click().sendKeys(value).build().perform();
			} catch (Exception ex) {
				Report.Fail(driver, "Element not visible");
			}

		}
		
		public boolean ActionClick(WebDriver driver, WebElement obj, String PassMessage, String FailMessage) {
			try {
				if (isobjPresent(obj)) {
					System.out.println("Action done");
					Actions action = new Actions(driver);
					// WebElement element = driver.findElement((By) obj);
					action.moveToElement(obj);
					action.click().build().perform();
					Report.Pass(PassMessage);
					return true;
				} else
					Report.Fail(driver, FailMessage);

				return false;

			} catch (NoSuchElementException e) {
				Report.Fail(driver, FailMessage);

				return false;
			}
		}
		
		public boolean ActionDoubleClick(WebDriver driver, WebElement obj, String PassMessage, String FailMessage) {
			try {
				if (isobjPresent(obj)) {
					System.out.println("Action done");
					Actions action = new Actions(driver);
					// WebElement element = driver.findElement((By) obj);
					action.moveToElement(obj);
					action.doubleClick().build().perform();
					Report.Pass(PassMessage);
					return true;
				} else
					Report.Fail(driver, FailMessage);

				return false;

			} catch (NoSuchElementException e) {
				Report.Fail(driver, FailMessage);

				return false;
			}
		}

		public void navigateTo(WebDriver driver, String url) {
			driver.navigate().to(url);
		}
		
		public void navigateBack(WebDriver driver) {
			driver.navigate().back();
		}
		
		public  String getCurrentUrl(WebDriver driver) {
			return driver.getCurrentUrl();
		}
		
		public String getCurrentWindowID(WebDriver driver)
		{
			return driver.getWindowHandle();
		}
		
		public WebDriver OpenNewTab_FocusNewTab(WebDriver driver){
			try {
				driver.findElement(By.xpath("//body")).sendKeys(Keys.CONTROL+"t");
				
				ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
				
				SwitchWindow(driver, windows.get(1));			
				
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return driver;
		}
		
		public void SwitchWindow(WebDriver driver,String WinID)
		{
			driver.switchTo().window(WinID);
		}
		
		public void CloseCuurentWindow(WebDriver driver)
		{
			driver.close();
		}

		public boolean Refresh(WebDriver driver){
			boolean refresh = false;
			try{
				driver.navigate().refresh();
				refresh = true;
			}catch(Exception ex){
				ex.printStackTrace();
			}
			return refresh;
		}
		
		
		public boolean fnSwitchFrame(WebDriver driver, WebElement Obj) {
			boolean boolSwitch = false;
			try {

				driver.switchTo().frame(Obj);
				boolSwitch = true;

			} catch (Exception ex) {
				ex.printStackTrace();
				boolSwitch = false;
			}
			return boolSwitch;
		}

		public boolean fnSwitchContent(WebDriver driver) {
			boolean boolSwitch = false;
			try {

				driver.switchTo().defaultContent();
				boolSwitch = true;

			} catch (Exception ex) {
				ex.printStackTrace();
				boolSwitch = false;
			}
			return boolSwitch;
		}

		public static String getCurrentTime(){
			return new SimpleDateFormat("_MMddyyyy_HHmmss").format(Calendar.getInstance().getTime());
		}
		
		public static String getCurrentDate(){
			return new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
		}
		
		public static String getTestString() {
			return "test";
		}
}
